// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('resto', ['ionic'])
        .factory('Restaurants', function() {
            var all = function() {
                var restaurantString = window.localStorage['restaurants'];
                if (restaurantString) {
                    return angular.fromJson(restaurantString);
                }
                return [];
            };
            var load = function(key, liste) {
                if (liste === undefined) {
                    liste = all();
                }
                for (var i in liste) {
                    if (liste[i].id === key) {
                        return liste[i];
                    }
                }
            };
            var saveAll = function(restaurants) {
                window.localStorage['restaurants'] = angular.toJson(restaurants);
            }
            var getServer = function() {
                return window.localStorage['server'];
            }
            return {
                all: function() {
                    return all();
                },
                insert: function(restaurant, $http, method) {
                    $http.jsonp(getServer()+'/restau.middleware/restau/restaurant/create?callback=JSON_CALLBACK', {
                        params : {
                            'name': restaurant.name,
                            'description': restaurant.description,
                        }
                    }).
                        success(function(data) {
                            restaurant.id = data.message.id;
                            var liste = all();
                            liste.push(restaurant);
                            saveAll(liste);
                        }).then(method);
                },
                update: function(restaurant, $http, method) {
                    
                    $http.jsonp(getServer()+'/restau.middleware/restau/restaurant/update?callback=JSON_CALLBACK', {
                        params : {
                            'id': restaurant.id,
                            'name': restaurant.name,
                            'description': restaurant.description,
                        }
                    })
                        .success(function(data) {
                            var liste = all();
                            var resto = load(restaurant.id, liste);
                            resto.name = restaurant.name;
                            resto.description = restaurant.description;
                            saveAll(liste);
                        }).then(method);
                },
                delete: function(restaurant, $http, method) {
                    $http.jsonp(getServer()+'/restau.middleware/restau/restaurant/delete?callback=JSON_CALLBACK', {
                        params : {
                            'id': restaurant.id,
                        }
                    })
                        .success(function(data) {
                            var liste = all();
                            var resto = load(restaurant.id, liste);
                            liste.pop(resto);
                            saveAll(liste);
                        }).then(method);
                },
                load: function(key) {
                    return load(key);
                },
                newRestaurant: function(restaurant) {
                    return {
                        name : restaurant.name,
                        description : restaurant.description,
                    };
                },
                resetRestaurant: function(restaurant) {
                    restaurant.id = "";
                    restaurant.name = "";
                    restaurant.description = "";
                },
                reloadAll : function($http, method) {
                    // load the restaurants from the external server and save the results locally
                    $http.jsonp(getServer()+'/restau.middleware/restau/restaurant/getall?callback=JSON_CALLBACK').
                        success(function(data) {
                            window.localStorage['restaurants'] = data.message;
                        }).then(method);
                }
            }
        })
        
        .run(function($ionicPlatform) {
            $ionicPlatform.ready(function() {
                if (window.cordova && window.cordova.plugins.Keyboard) {
                    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
                    // for form inputs)
                    cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

                    // Don't remove this line unless you know what you are doing. It stops the viewport
                    // from snapping when text inputs are focused. Ionic handles this internally for
                    // a much nicer keyboard experience.
                    cordova.plugins.Keyboard.disableScroll(true);
                }
                if (window.StatusBar) {
                    StatusBar.styleDefault();
                }
            });
        })

        .controller('RestoCtrl', function($scope, $timeout, $ionicModal, Restaurants, $http) {
            $scope.restaurants = Restaurants.all();
            // Create our modal
            $ionicModal.fromTemplateUrl('new_restaurant.html', function(modal) {
                $scope.restaurantModal = modal;
            }, {
                scope: $scope
            });
            
            $ionicModal.fromTemplateUrl('update_restaurant.html', function(modal) {
                $scope.restaurantUpdateModal = modal;
            }, {
                scope: $scope
            });
            // Load or initialize restaurants
            $scope.createResto = function(restaurant) {
                if (!restaurant.name) {
                    return;
                }
                var newRestaurant = Restaurants.newRestaurant(restaurant);
                Restaurants.insert(newRestaurant, $http, function(){
                    $scope.restaurants = Restaurants.all();
                    $scope.restaurantModal.hide();
                    Restaurants.resetRestaurant(restaurant);
                });
            };
            
            $scope.updateResto = function(restaurant) {
                if (!restaurant.name) {
                    return;
                }
                Restaurants.update(restaurant, $http, function() {
                    $scope.restaurants = Restaurants.all();
                    $scope.restaurantUpdateModal.hide();
                    Restaurants.resetRestaurant(restaurant);
                });
            };

            $scope.newRestaurant = function() {
                $scope.restaurantModal.show();
            };
            
            $scope.loadRestaurant = function(restaurant) {
                $scope.restaurantUpdate = Restaurants.load(restaurant.id);
                $scope.restaurantUpdateModal.show();
            };
            
            $scope.deleteRestaurant = function(restaurant) {
                Restaurants.delete(restaurant, $http, function() {
                    $scope.restaurants = Restaurants.all();
                    Restaurants.resetRestaurant(restaurant);
                });
            };

            $scope.closeNewRestaurant = function() {
                $scope.restaurantModal.hide();
            };
            
            $scope.closeUpdateRestaurant = function() {
                $scope.restaurantUpdateModal.hide();
            };
            
            $scope.reloadAll = function() {
                Restaurants.reloadAll($http, function(){
                    $scope.restaurants = Restaurants.all();
                    $scope.$broadcast('scroll.refreshComplete'); 
                })
            };
            
            $timeout(function() {
                while(true) {
                    var server = prompt('Server address:');
                    if(server) {
                      window.localStorage['server'] = server;
                      break;
                    }
                  }
              });

        });